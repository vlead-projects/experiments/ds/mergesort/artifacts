var globeCreator = function (divEle) {

	var body = document.body;
	var div_element = document.createElement("div");
	div_element.className = "content-wrapper";

	var div_element1 = document.createElement("div");
	div_element1.className = "bond";

	var div_element2 = document.createElement("div");
	div_element2.className = "form-group";

	var newInput = document.createElement("input");
	newInput.id = "arr";
	newInput.className = "form-control";
	newInput.setAttribute("type", "text");
	div_element2.appendChild(newInput);

	var btn1 = document.createElement("button");
	btn1.id = "but1";
	btn1.className = "btn btn-success btn-lg";
	btn1.addEventListener("click", function(){
		sort();
	});
	btn1.innerText = "Sort";
	var btn2 = document.createElement("button");
	btn2.innerText = "Reset";
	btn2.id = "but2";
	btn2.className = "btn btn-success btn-lg";
	btn2.addEventListener("click", function(){
		reset2();
	});
	div_element2.appendChild(btn1);
	div_element2.appendChild(btn2);
	div_element1.appendChild(div_element2);
	div_element.appendChild(div_element1);
	

	var div_element3 = document.createElement("div");;
	div_element3.className = "container-fluid";
	var div_element4 = document.createElement("div");
	div_element4.className = "card mb-3";
	
	var newsvg = document.createElementNS("http://www.w3.org/2000/svg","svg");
	newsvg.id = "svg-canvas";
	newsvg.setAttribute("width", "100%");
	newsvg.setAttribute("height","600");
	
	div_element4.appendChild(newsvg);
	div_element3.appendChild(div_element4);
	div_element.appendChild(div_element3);
	divEle.appendChild(div_element);

	console.log(body);

	var node = {};
	var vert = [];
	var mark = [];

	function sort() {
		reset2();
		var array = document.getElementById('arr').value;
		var arr = array.split(" ");
		array = array.trim();
		console.log(array);
		merge(array, 1);
		mark[1] = 1;
		drawGraph();
	}

	function merge(arr, i) {
		if (arr.length >= 1) {
			vert.push(arr);
			node[i] = arr;
			mark[i] = 1;
			var tp1 = arr.split(' ');
			console.log(tp1);
			tp1.sort();
			var tp2 = tp1.join(" ");
			console.log(tp2);
			node[-i] = tp2;
			vert.push(tp2);
		}
	}

	function drawGraph() {
		for (v in node) {
			g.setNode(v, {
				label: node[v],
				class: "type-TOP"
			});
		}


		for (v in node) {
			if (v > 0) {
				if (mark[v] == 0) // that means no edge from i to -i instead from i to 2*i and 2*i+1
				{
					g.setEdge(v, 2 * v, {
						label: "Split"
					});
					g.setEdge(v, 2 * v + 1, {
						label: "Split"
					});
				} else //that means edge from i to -i
				{
					g.setEdge(v, -v, {
						label: "MS"
					});
				}
			} else {
				var dest = Math.floor(v / -2);
				console.log(v, dest);
				if (dest > 0) {
					g.setEdge(v, -dest, {
						label: "Merge"
					});
				}
			}
		}
		// console.log("Works till here");
		draw();

	}

	var g = new dagreD3.graphlib.Graph()
		.setGraph({})
		.setDefaultEdgeLabel(function () {
			return {};
		});

	function draw() {
		g.nodes().forEach(function (v) {
			var node = g.node(v);
			// Round the corners of the nodes
			node.rx = node.ry = 5;
		})

		// Create the renderer
		var render = new dagreD3.render();

		// Set up an SVG group so that we can translate the final graph.
		var svg = d3.select("svg"),
			svgGroup = svg.append("g");

		// Run the renderer. This is what draws the final graph.
		render(d3.select("svg g"), g);

		d3.select("svg g").selectAll("g.node")
			.on("click", function (id) {
				if (id > 0) {
					if (mark[id] == 1) //node is not opened
					{
						var _node = g.node(id);
						mark[id] = 0;
						console.log(id, _node);
						var array = _node.label;
						console.log(array);
						var mid = Math.floor(array.length / 2);
						var tp1 = array.substr(0, mid);
						var tp2 = array.substr(mid, array.length - mid);
						merge(tp1, 2 * id);
						merge(tp2, 2 * id + 1);
					} else if (mark[id] == 0) {
						if (mark[2 * id] == 1 && mark[2 * id + 1] == 1) {
							var _node = g.node(id);
							mark[id] = 1;
							console.log(node);
							delete node[2 * id];
							delete node[-(2 * id)];
							delete node[2 * id + 1];
							delete node[-(2 * id + 1)];
						}

					}
					reset();
					drawGraph();
				}
			});

		// Center the graph
		var xCenterOffset = (svg.attr("width") - g.graph().width) / 2;
		svgGroup.attr("transform", "translate(" + xCenterOffset + ", 20)");
		svg.attr("height", g.graph().height + 40);

	}

	function reset() {
		g = null
		g = new dagreD3.graphlib.Graph()
			.setGraph({})
			.setDefaultEdgeLabel(function () {
				return {};
			});

	}

	function reset1() {
		g = null
		g = new dagreD3.graphlib.Graph()
			.setGraph({})
			.setDefaultEdgeLabel(function () {
				return {};
			});
		node = {};
		vert = [];
		mark = [];
		render = null
		svg = null
		document.getElementById('svg-canvas').innerHTML = null
		document.getElementById('arr').value = null

	}

	function reset2() {
		g = null
		g = new dagreD3.graphlib.Graph()
			.setGraph({})
			.setDefaultEdgeLabel(function () {
				return {};
			});
		node = {};
		vert = [];
		mark = [];
		render = null
		svg = null
		document.getElementById('svg-canvas').innerHTML = null
	}
}